#!/usr/bin/env python3
# coding: utf-8

"""
======================
Load and eval an agent
======================

This example shows how to load and assess a trained agent.
"""

###############################################################################
# Import required packages

from rlenv.envs.wall.core import AccentaEnv
import rlagent


###############################################################################
# Load the agent

agent = rlagent.load_agent()


###############################################################################
# Assess the agent

score = AccentaEnv.eval(agent)
print(score)