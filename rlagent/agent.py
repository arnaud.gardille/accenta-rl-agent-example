
from rlenv.envs.wall.core import AccentaEnv

import numpy as np

def degree_celsius_to_kelvin(degree_celsius):
    return degree_celsius + 273.15

ETA = 0.25 
RHO = 0.9   
DELTA_TEMP_COND = 5.
TEMP_BAT_IN_K = degree_celsius_to_kelvin(35.)
TEMP_BAT_OUT_K = TEMP_BAT_IN_K - DELTA_TEMP_COND


def efficiency_func(current_weather_degree_celsius, eta=ETA, cop_max=8.):
    current_weather_kelvin = degree_celsius_to_kelvin(current_weather_degree_celsius)

    if current_weather_kelvin >= TEMP_BAT_OUT_K:
        cop_carnot = float("inf")
    else:
        cop_carnot = TEMP_BAT_OUT_K / (TEMP_BAT_OUT_K - current_weather_kelvin)

    cop = cop_carnot * ETA
    cop = min(cop, cop_max)   # COP can't be > than 8

    return cop

class SubmittableAgent:
    def __init__(self, agent, simplify=True) -> None:
        self.agent = agent
        self.simplify = simplify

    def predict(self, obs):
        obs = np.array(obs,  dtype=np.float32)
        temperature, needs, storage = obs[0], obs[1], obs[2]

        if len(obs) != 11:
            obs = np.zeros(11)

        action, _ = self.agent.predict(obs)

        efficiency = efficiency_func(temperature)


        if self.simplify:
            assert np.size(action) == 2, action
    
            stock, destock = action[0], action[1]

            stock = min(stock, (500. - storage)/efficiency)
            
            destock = min(destock, storage, needs)
    
            if RHO < efficiency:
                heat_elec = (needs - float(destock))/efficiency
            else:
                print("Temperature extemement basse!")
                heat_elec = 0.


            action = [max(float(heat_elec), 0.), max(float(stock), 0.), max(float(destock), 0.)]
            #np.array([a1, a2, a3],  dtype=np.float32)

        assert np.size(action) == 3

        return action, _
        

class StockFor24HoursAgent:
    def __init__(self, use_security_ratio=True) -> None:
        self.current_hour = 0
        self.shopping_hour = 14 #On achete a 14h
        self.past_consumption = []
        self.store_from = 1008
        self.unstore_from = 6000

        self.objective = 50
        self.use_security_ratio = use_security_ratio

    def predict(self, obs):

        #if self.store_from

        obs = np.array(obs,  dtype=np.float32)

        self.current_hour += 1
        self.current_hour %= 24
        
        temperature, needs, storage = obs[0], obs[1], obs[2]

        security_ratio = temperature/10.-1.8

        self.past_consumption.append(needs)
        estimated_consumption = sum(self.past_consumption[-24:])
        
        stock = 0.
        if self.current_hour == self.shopping_hour:
            efficiency = efficiency_func(temperature)
            stock = estimated_consumption/efficiency
            if self.use_security_ratio:
                stock += security_ratio*(500-(storage+stock*efficiency))/500.
                #stock += security_ratio**2 *np.sqrt((500-(storage+stock*efficiency))/500.)
        destock = min(needs, storage)

        return [stock, destock], None
   
    
class NoStorageAgent:
    def predict(self, obs):
        temperature, needs, storage = obs[0], obs[1], obs[2]

        efficiency = efficiency_func(temperature)
        heat = efficiency*needs

        return [heat, 0., 0.], None
    

def load_agent():
    """
    This function is mandatory for the ENS Challenge Data evaluation platform to evaluate your agent.

    This function serves as a single, common entry point for all groups to load your trained agent.
    Do not delete it or change its signature (i.e. do not add an argument to it) otherwise your agent will be rejected by the ENS evaluation platform.

    Here an example is provided to load a PPO agent whose weights are loaded from the file returned by the rlagent.data.ppo_trained_model_example_path() function.
    You have to adapt the content of this function to load your **pre-trained** agent.

    Here an example is provided to load a PPO agent whose weights are loaded from the file returned by the rlagent.data.ppo_trained_model_example_path() function.
    You have to adapt the content of this function to load your** pre-trained agent.

    WARNING: this function must return a pre-trained model (e.g. saved in a file included in the git repo, downloaded from the internet, etc.).
    You should not train a model here because there is a timeout on the execution time of this function on the ENS Challenge Data evaluation platform.

    Normally you would load a pre-trained model file, instantiate it and give it as the return value of this function.

    Example code for training an agent is available in the examples directory. You can use them as inspiration to define and train your own agent.

    Returns
    -------
    Gym agent
        The pre-trained model to be evaluated on the ENS Challenge Data platform.
    """    
    
    

    
    return SubmittableAgent(StockFor24HoursAgent())
