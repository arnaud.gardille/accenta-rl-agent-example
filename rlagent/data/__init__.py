import os

__all__ = [
            'ppo_trained_model_example_path',
          ]

def expand_path(path):
    """Expand `~` constructs and return an absolute path.

    On Unix operating systems (Linux, MacOSX, ...),
    `~` is a shortcut for the curent user's home directory
    (e.g. "/home/jack" for "jack").

    Example
    -------

    Lets assume we are on a Unix system,
    the current user is "jack" and the current path is "/bar".

    >>> expand_path("~/foo")
    "/home/jack/foo"

    >>> expand_path("baz")
    "/bar/baz"

    >>> expand_path("../tmp")
    "/tmp"

    Parameters
    ----------
    path : str
        The `path` to expand.

    Returns
    -------
    str
        The absolute and expanded `path`.
    """
    path = os.path.expanduser(path)  # to handle "~/..." paths
    path = os.path.abspath(path)     # to handle relative paths
    return path


DIR_PATH = os.path.abspath(os.path.dirname(__file__))

def ppo_trained_model_example_path() -> str:
    """
    The filepath of a trained model provided with the library for documentation.

    Returns
    -------
    str
        The filepath of a trained model.
    """
    return expand_path(os.path.join(DIR_PATH, "trained_model.zip"))